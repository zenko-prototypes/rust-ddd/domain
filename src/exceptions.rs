use std::error::Error;

pub mod aggregates;
pub mod entities;
pub mod value_objects;

pub trait DomainException: Error + PartialEq + Eq {}
