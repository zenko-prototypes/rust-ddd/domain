use uuid::Uuid;

use crate::{
    aggregates::root::AggregateRoot,
    domain_events::DomainEvent,
    value_objects::articles::{id::ArticleId, title::ArticleTitle},
};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArticleTitleUpdatedData {
    title: ArticleTitle,
}

impl ArticleTitleUpdatedData {
    pub fn new(title: ArticleTitle) -> Self {
        Self { title }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArticleTitleUpdated {
    /// Id of the event
    id: Uuid,
    aggregate_id: ArticleId,
    data: ArticleTitleUpdatedData,
}

impl ArticleTitleUpdated {
    pub fn new(aggregate_id: ArticleId, data: ArticleTitleUpdatedData) -> Self {
        Self {
            id: Uuid::new_v4(),
            aggregate_id,
            data,
        }
    }
}

impl DomainEvent<ArticleTitleUpdatedData> for ArticleTitleUpdated {
    type Aggregate = AggregateRoot;
    fn id(&self) -> Uuid {
        self.id
    }

    fn data(&self) -> ArticleTitleUpdatedData {
        self.data.clone()
    }

    /// Apply this event on the aggregate without validation
    fn apply(&self, aggregate: &mut Self::Aggregate) {
        aggregate
            .article_mut()
            .as_mut()
            .unwrap()
            .set_title(self.data.title.clone());
    }
}
