use uuid::Uuid;

use crate::aggregates::root::AggregateRoot;
use crate::domain_events::DomainEvent;
use crate::entities::articles::Article;
use crate::value_objects::articles::id::ArticleId;
use crate::value_objects::articles::title::ArticleTitle;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArticleCreatedData {
    id: ArticleId,
    title: ArticleTitle,
}

impl ArticleCreatedData {
    pub fn new(id: ArticleId, title: ArticleTitle) -> Self {
        Self { id, title }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArticleCreated {
    /// Id of the event
    id: Uuid,
    aggregate_id: ArticleId,
    data: ArticleCreatedData,
}

impl ArticleCreated {
    pub fn new(aggregate_id: ArticleId, data: ArticleCreatedData) -> Self {
        Self {
            id: Uuid::new_v4(),
            aggregate_id,
            data,
        }
    }
}

impl DomainEvent<ArticleCreatedData> for ArticleCreated {
    type Aggregate = AggregateRoot;
    fn id(&self) -> Uuid {
        self.id
    }

    fn data(&self) -> ArticleCreatedData {
        self.data.clone()
    }

    /// Apply this event on the aggregate without validation
    fn apply(&self, aggregate: &mut Self::Aggregate) {
        let article = Article::new(self.data.id.clone(), self.data.title.clone());

        aggregate.set_article(Some(article).clone());
    }
}
