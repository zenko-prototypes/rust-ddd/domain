use self::{created::ArticleCreated, title_updated::ArticleTitleUpdated};

pub mod created;
pub mod title_updated;

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub enum ArticleEvent {
    Created(ArticleCreated),
    TitleUpdated(ArticleTitleUpdated),
    #[default]
    Nil,
}
