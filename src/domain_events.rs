use uuid::Uuid;

use crate::aggregates::Aggregate;

pub mod articles;

pub trait DomainEvent<D>: Clone + PartialEq + Eq {
    type Aggregate: Aggregate;

    fn id(&self) -> Uuid;
    fn data(&self) -> D;

    /// Apply this event on the aggregate without validation
    fn apply(&self, aggregate: &mut Self::Aggregate);
}
