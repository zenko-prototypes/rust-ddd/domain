use uuid::Uuid;

use crate::{
    exceptions::value_objects::articles::id::ArticleIdException, value_objects::ValueObject,
};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArticleId(Uuid);

impl ValueObject<Uuid> for ArticleId {
    type Error = ArticleIdException;

    /// Validate the domain rule to create a article id
    fn new(value: Uuid) -> Result<ArticleId, <Self as ValueObject<Uuid>>::Error> {
        if value.is_nil() {
            return Err(ArticleIdException::Nil);
        }
        Ok(ArticleId(value))
    }

    fn value(self) -> Uuid {
        self.0
    }
}

impl TryFrom<Uuid> for ArticleId {
    type Error = ArticleIdException;

    fn try_from(value: Uuid) -> Result<Self, Self::Error> {
        ValueObject::new(value)
    }
}

impl Into<Uuid> for ArticleId {
    fn into(self) -> Uuid {
        self.value()
    }
}
