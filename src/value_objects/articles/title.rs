use crate::{
    exceptions::value_objects::articles::title::ArticleTitleException, value_objects::ValueObject,
};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArticleTitle(String);

impl ValueObject<String> for ArticleTitle {
    type Error = ArticleTitleException;

    /// Validate the domain rule to create a article title
    fn new(value: String) -> Result<ArticleTitle, <Self as ValueObject<String>>::Error> {
        if value.is_empty() {
            return Err(ArticleTitleException::Empty);
        }
        Ok(ArticleTitle(value))
    }

    fn value(self) -> String {
        self.0
    }
}

impl TryFrom<String> for ArticleTitle {
    type Error = ArticleTitleException;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        ValueObject::new(value)
    }
}

impl Into<String> for ArticleTitle {
    fn into(self) -> String {
        self.value()
    }
}
