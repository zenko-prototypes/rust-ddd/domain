pub mod articles;

pub trait Entity<T>: Clone + PartialEq + Eq {
    fn id(self) -> T;
}
