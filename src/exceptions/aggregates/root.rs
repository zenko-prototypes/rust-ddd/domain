use thiserror::Error;

use crate::{exceptions::DomainException, value_objects::articles::id::ArticleId};

#[derive(Error, Debug, PartialEq, Eq)]
pub enum AggregateRootException {
    #[error("Article is already set for aggregate root: {id:?}")]
    ArticleAlreadySet { id: ArticleId },
    #[error("No article was created for this aggregate")]
    NoArticleSet,
}

impl DomainException for AggregateRootException {}
