use thiserror::Error;

use crate::exceptions::DomainException;

#[derive(Error, Debug, PartialEq, Eq)]
pub enum ArticleTitleException {
    #[error("Article title cannot be empty")]
    Empty,
}

impl DomainException for ArticleTitleException {}
