use thiserror::Error;

use crate::exceptions::DomainException;

#[derive(Error, Debug, PartialEq, Eq)]
pub enum ArticleIdException {
    #[error("Article id cannot be nil")]
    Nil,
}

impl DomainException for ArticleIdException {}
