pub mod articles;

pub trait ValueObject<T>: Sized + TryFrom<T> + Into<T> + Clone + PartialEq + Eq {
    type Error;

    /// Validate the domain rule to create a value object
    fn new(value: T) -> Result<Self, <Self as ValueObject<T>>::Error>;

    fn value(self) -> T;
}
