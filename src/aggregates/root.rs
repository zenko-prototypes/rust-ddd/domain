use crate::{
    domain_events::{
        articles::{
            created::{ArticleCreated, ArticleCreatedData},
            title_updated::{ArticleTitleUpdated, ArticleTitleUpdatedData},
            ArticleEvent,
        },
        DomainEvent,
    },
    entities::{articles::Article, Entity},
    exceptions::aggregates::root::AggregateRootException,
    value_objects::articles::{id::ArticleId, title::ArticleTitle},
};

use super::{Aggregate, AggregateBuilder};

/// Root aggregate for the domain
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct AggregateRoot {
    /// Indicate if the version is already incremented once
    version_incremented: bool,
    /// Current version of the aggregate
    version: u8,

    /// List of all the events that have not been persisted
    events: Vec<ArticleEvent>,

    /// Main entity of the aggregate
    article: Option<Article>,
}

impl AggregateRoot {
    pub fn article(&self) -> Option<&Article> {
        self.article.as_ref()
    }

    pub(crate) fn article_mut(&mut self) -> &mut Option<Article> {
        &mut self.article
    }

    pub(crate) fn set_article(&mut self, article: Option<Article>) {
        self.article = article;
    }

    /// Validate the domain rule to create a article and update the aggregate consequently
    pub fn create_article(
        &mut self,
        id: ArticleId,
        title: ArticleTitle,
    ) -> Result<(), AggregateRootException> {
        // Verification
        match &self.article {
            Some(article) => {
                return Err(AggregateRootException::ArticleAlreadySet {
                    id: article.clone().id(),
                })
            }
            None => (),
        };

        // Creation and handle of the event to update the aggregate
        let event_data = ArticleCreatedData::new(id.clone(), title);
        let event = ArticleEvent::Created(ArticleCreated::new(id, event_data));
        self.handle_event(event, true);

        Ok(())
    }

    /// Validate the domain rule to update a article title and update the aggregate consequently
    pub fn update_article_title(
        &mut self,
        title: ArticleTitle,
    ) -> Result<(), AggregateRootException> {
        // Verification
        match &self.article {
            Some(_) => (),
            None => return Err(AggregateRootException::NoArticleSet),
        };

        // Creation and handle of the event to update the aggregate
        let event_data = ArticleTitleUpdatedData::new(title);
        let event = ArticleEvent::TitleUpdated(ArticleTitleUpdated::new(self.id(), event_data));
        self.handle_event(event, true);

        Ok(())
    }
}

impl Aggregate for AggregateRoot {
    type Events = ArticleEvent;
    /// The id of the aggregate is the id of the main entity
    type Id = ArticleId;

    fn id(&self) -> Self::Id {
        self.article.as_ref().unwrap().clone().id()
    }

    fn version(&self) -> u8 {
        self.version
    }

    fn version_incremented(&self) -> bool {
        self.version_incremented
    }

    fn increment_version(&mut self) {
        if self.events.is_empty() && !self.version_incremented {
            self.version_incremented = true;
            self.version += 1;
        }
    }

    fn events(&self) -> Vec<Self::Events> {
        self.events.clone()
    }

    fn clear_events(&mut self) {
        self.events.clear();
    }

    /// Handle the events and increment the version of the aggregate if necessary
    fn handle_event(&mut self, event: Self::Events, increment_version: bool) {
        match event.clone() {
            Self::Events::Created(created) => created.apply(self),
            Self::Events::TitleUpdated(title_updated) => title_updated.apply(self),
            Self::Events::Nil => return,
        };
        if increment_version {
            self.increment_version();
        }
        self.events.push(event);
    }
}

#[derive(Default)]
pub struct AggregateRootBuilder<E> {
    version: u8,
    events: Vec<E>,
}

impl AggregateBuilder for AggregateRootBuilder<ArticleEvent> {
    type Aggregate = AggregateRoot;
    type Events = ArticleEvent;

    /// Override the version of the aggregate that will be build
    fn with_version(&mut self, version: u8) -> &mut Self {
        self.version = version;
        self
    }

    /// Override the events list for the aggregate that will be build
    fn with_events(&mut self, events: Vec<ArticleEvent>) -> &mut Self {
        self.events = events;
        self
    }

    /// Add a event to aggregate that will be build
    fn add_event(&mut self, event: ArticleEvent) -> &mut Self {
        self.events.push(event);
        self
    }

    /// Build a aggregate root from the data given to the builder
    /// Also handle every events without incrementing the version of the aggregate root
    fn build(&self) -> Self::Aggregate {
        let mut aggregate = AggregateRoot {
            version_incremented: false,
            version: self.version,
            events: vec![],
            article: None,
        };

        for event in self.events.clone() {
            aggregate.handle_event(event, false);
        }

        aggregate.clear_events();
        aggregate
    }
}
