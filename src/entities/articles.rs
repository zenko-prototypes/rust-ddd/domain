use crate::value_objects::articles::{id::ArticleId, title::ArticleTitle};

use super::Entity;

#[derive(Debug, Clone, Eq)]
pub struct Article {
    id: ArticleId,
    title: ArticleTitle,
}

impl Article {
    pub(crate) fn new(id: ArticleId, title: ArticleTitle) -> Article {
        Article { id, title }
    }

    pub fn title(self) -> ArticleTitle {
        self.title
    }

    pub fn set_title(&mut self, title: ArticleTitle) {
        self.title = title;
    }
}

impl Entity<ArticleId> for Article {
    fn id(self) -> ArticleId {
        self.id
    }
}

impl PartialEq for Article {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}
