pub mod root;

pub trait Aggregate: Clone + PartialEq + Eq {
    type Events;
    /// The id of the aggregate is the id of the main entity
    type Id;

    fn id(&self) -> Self::Id;

    fn version(&self) -> u8;
    fn version_incremented(&self) -> bool;
    fn increment_version(&mut self);

    fn events(&self) -> Vec<Self::Events>;
    fn clear_events(&mut self);
    /// Handle the events and increment the version of the aggregate if necessary
    fn handle_event(&mut self, event: Self::Events, increment_version: bool);
}

pub trait AggregateBuilder: Default {
    type Aggregate: Aggregate;
    type Events;

    /// Override the version of the aggregate that will be build
    fn with_version(&mut self, version: u8) -> &mut Self;
    /// Override the events list for the aggregate that will be build
    fn with_events(&mut self, events: Vec<Self::Events>) -> &mut Self;

    /// Add a event to aggregate that will be build
    fn add_event(&mut self, event: Self::Events) -> &mut Self;

    /// Build a aggregate root from the data given to the builder
    /// Also handle every events without incrementing the version of the aggregate
    fn build(&self) -> Self::Aggregate;
}
