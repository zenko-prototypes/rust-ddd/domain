extern crate domain;

use domain::{
    aggregates::{root::AggregateRootBuilder, Aggregate, AggregateBuilder},
    domain_events::articles::{
        created::{ArticleCreated, ArticleCreatedData},
        ArticleEvent,
    },
    entities::Entity,
    value_objects::{
        articles::{id::ArticleId, title::ArticleTitle},
        ValueObject,
    },
};
use uuid::Uuid;

#[test]
fn create_article_for_empty_aggregate() {
    let mut aggregate = AggregateRootBuilder::default().with_version(0).build();

    let id = ArticleId::new(Uuid::new_v4());
    assert!(id.is_ok());

    let title = ArticleTitle::new("New Title".to_string());
    assert!(title.is_ok());

    let result = aggregate.create_article(
        id.as_ref().unwrap().clone(),
        title.as_ref().unwrap().clone(),
    );
    assert!(result.is_ok());

    assert_eq!(aggregate.id(), id.as_ref().unwrap().clone());

    assert_eq!(aggregate.events().len(), 1);
    assert!(aggregate.events().iter().any(|event| match event {
        ArticleEvent::Created(_) => true,
        _ => false,
    }));

    assert_eq!(aggregate.version(), 1);
    assert_eq!(aggregate.version_incremented(), true);

    assert_eq!(aggregate.article().unwrap().clone().id(), id.unwrap());
    assert_eq!(aggregate.article().unwrap().clone().title(), title.unwrap());
}

#[test]
fn update_article_title_for_aggregate() {
    let id = ArticleId::new(Uuid::new_v4());
    assert!(id.is_ok());

    let title = ArticleTitle::new("New Title".to_string());
    assert!(title.is_ok());

    let mut aggregate = AggregateRootBuilder::default()
        .with_version(1)
        .add_event(ArticleEvent::Created(ArticleCreated::new(
            id.as_ref().unwrap().clone(),
            ArticleCreatedData::new(id.as_ref().unwrap().clone(), title.unwrap()),
        )))
        .build();

    let new_title = ArticleTitle::new("New Title".to_string());
    assert!(new_title.is_ok());

    let result = aggregate.update_article_title(new_title.as_ref().unwrap().clone());
    assert!(result.is_ok());

    assert_eq!(aggregate.id(), id.as_ref().unwrap().clone());

    assert_eq!(aggregate.events().len(), 1);
    assert!(aggregate.events().iter().any(|event| match event {
        ArticleEvent::TitleUpdated(_) => true,
        _ => false,
    }));

    assert_eq!(aggregate.version(), 2);
    assert_eq!(aggregate.version_incremented(), true);

    assert_eq!(aggregate.article().unwrap().clone().id(), id.unwrap());
    assert_eq!(
        aggregate.article().unwrap().clone().title(),
        new_title.unwrap()
    );
}
